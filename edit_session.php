<?php
  include 'globals.php';
    include 'read_files.php';

    $session_data = read_session($data_path, $_GET['code']);
 ?>

<html>
  <head>
    <title>Sửa chương trình <?php echo $session_data['name'] ?></title>
    <link rel="stylesheet" type="text/css" href="add.css" />
    <script
  src="http://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="main-box">
      <form action="add_session.php" method="post">
        <div class="left-box">
          <div class="form-group">
            <label>Code chương trình:</label>
            <input type="text" name="code" value="<?php echo $session_data['code'] ?>" />
          </div>
          <div class="form-group">
            <label>Tên chương trình:</label>
            <input type="text" name="name"  value="<?php echo $session_data['name'] ?>"/>
          </div>
          <div class="form-group">
            <label>Ngày:</label>
            <input type="date" name="date" value="<?php echo date('Y-m-d', $session_data['date']) ?>"/>
          </div>
          <div class="form-group">
            <label>Mô tả:</label><br />
            <textarea name="desc"><?php echo $session_data['desc'] ?></textarea>
          </div>
          <div class="form-group">
            <label>Danh sách tham gia:</label><br />
            <textarea id="join-list" name="joinlist"><?php echo $session_data['joinlist'] ?></textarea>
          </div>
        </div>
        <div class="right-box">
          <div class="right-top">
            <?php
              foreach ($session_data['rewards'] as $reward) {
                echo '<div class="reward-container">
                  <div class="left-part">
                    <button type="button" class="btn-delete-reward">X</button>
                  </div>
                  <div class="right-part">
                    <div class="form-group">
                      <label>Giải thưởng:</label>
                      <input type="text" name="reward[]" value="'.$reward['reward'].'"/>
                    </div>
                    <div class="form-group">
                      <label>Số lượng</label>
                      <input type="number" name="quan[]" value="'.$reward['quan'].'"/>
                    </div>
                  </div>
                </div>';
              }
             ?>
          </div>
          <div class="right-bottom">
            <button id="btn-add-reward" type="button">Thêm giải</button>
            <button id="btn-add-session">Sửa chương trình</button>
          </div>
        </div>
      </form>
    </div>

    <script>
      $('#btn-add-reward').on('click', function() {
        $('.right-top').append('<div class="reward-container">\
          <div class="left-part">\
            <button type="button" class="btn-delete-reward">X</button>\
          </div>\
          <div class="right-part">\
            <div class="form-group">\
              <label>Giải thưởng:</label>\
              <input type="text" name="reward[]"/>\
            </div>\
            <div class="form-group">\
              <label>Số lượng</label>\
              <input type="number" name="quan[]" />\
            </div>\
          </div>\
        </div>')
      });

      $(document).on('click', '.btn-delete-reward',  function() {
        $(this).closest('.reward-container').remove();
      });
    </script>
  </body>
</html>
