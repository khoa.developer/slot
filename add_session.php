<?php
  include 'globals.php';

  if (!is_dir($data_path."\\".$_POST['code'])) {
    mkdir($data_path."\\".$_POST['code']);
  }

  if ($_POST['action'] == 'add') {
    $file = fopen($data_path.'\\'.$_POST['code'].'\\'."info.txt", "w");
    fwrite($file, $_POST['code']."\n");
    fwrite($file, $_POST['name']."\n");
    fwrite($file, $_POST['date']."\n");
    fwrite($file, $_POST['desc']."\n");
    fwrite($file, "-----------------------"."\n");
    for ($i=0; $i < count($_POST['reward']); $i++) {
      fwrite($file, $_POST['reward'][$i]."\n");
      fwrite($file, $_POST['quan'][$i]."\n");
    }
    fclose($file);

    $file = fopen($data_path.'\\'.$_POST['code'].'\\'."list.txt", "w");
    fwrite($file, $_POST['joinlist']);

    fclose($file);

  } else {
    $remains = array();

    $lines = file($data_path."\\".$_POST['code']."\\"."list.txt");
    foreach ($lines as $line) {
      if (strcmp(trim($line), trim($_POST['win'])) != 0) {
        $remains[] = trim($line);
      }
    }

    $file = fopen($data_path."\\".$_POST['code']."\\"."list.txt", "w");
    foreach($remains as $line){
      fwrite($file, $line.PHP_EOL);
    }

    fclose($file);

    $file = fopen($data_path."\\".$_POST['code']."\\"."winners.txt", "w");
    $reward_index = 0;
    foreach ($_POST['rewards'] as $reward) {
      if (array_key_exists('winners', $reward)) {
        foreach ($reward['winners'] as $winner) {
          fwrite($file, $reward_index.":".$winner."\n");
        }
      }
      $reward_index += 1;
    }
    fclose($file);
    //$lines = file($data_path."\\".$_POST['code']."\\"."winners.txt"); 
    //$last = sizeof($lines) - 1 ; 
    //unset($lines[$last]); 

    // write the new data to the file 
    //$fp = fopen($data_path."\\".$_POST['code']."\\"."winners.txt", 'w'); 
    //fwrite($fp, implode('', $lines)); 
    //fclose($fp); 
  }
  if ($_POST['action'] == "add") {
    header("Location: manage.php");
    exit();
  } else {
    echo "true";
  }
 ?>
