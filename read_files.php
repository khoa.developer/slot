<?php

function array_sort($array, $on, $order=SORT_ASC)
{
  $new_array = array();
  $sortable_array = array();

  if (count($array) > 0) {
      foreach ($array as $k => $v) {
          if (is_array($v)) {
              foreach ($v as $k2 => $v2) {
                  if ($k2 == $on) {
                      $sortable_array[$k] = $v2;
                  }
              }
          } else {
              $sortable_array[$k] = $v;
          }
      }

      switch ($order) {
          case SORT_ASC:
              asort($sortable_array);
          break;
          case SORT_DESC:
              arsort($sortable_array);
          break;
      }

      foreach ($sortable_array as $k => $v) {
          $new_array[$k] = $array[$k];
      }
  }

  return $new_array;
}

  function read_session($path, $code) {
    $session_data = array();
    $lines = explode("\n", file_get_contents($path."\\".$code."\\"."info.txt"));
    $session_data["code"] = trim($lines[0]);
    $session_data["name"] = trim($lines[1]);
    $session_data["date"] = strtotime($lines[2]);
    $session_data["desc"] = trim($lines[3]);

    $i = 5;
    $session_data["rewards"] = array();
    while ($i < (count($lines) - 1)) {
      $reward = array();
      $reward['reward'] = trim($lines[$i]);
      $reward['quan'] = trim($lines[$i + 1]);

      $session_data["rewards"][] = $reward;
      $i+=2;
    }

    $session_data['joinlist'] = explode(PHP_EOL, file_get_contents($path."\\".$code."\\"."list.txt"));
    array_pop($session_data['joinlist']);

    if (is_file($path."\\".$code."\\"."winners.txt")) {
      $lines = explode("\n", file_get_contents($path."\\".$code."\\"."winners.txt"));

      foreach ($lines as $line) {
          if ($line == "") {continue;}
        $parts = explode(":", trim($line));
        $reward_index = strval($parts[0]);
        $winner = $parts[1];

        $session_data["rewards"][$reward_index]["winners"][] = $winner;
      }
    }

    return $session_data;
  }


  function read_data($path) {
    $codes = array_diff(scandir($path), array('..', '.'));

    $data = array();

    foreach ($codes as $c) {
      $slot_session = read_session($path, $c);
      $data[] = $slot_session;
    }

    return array_sort($data, 'date', SORT_DESC);
  }

 ?>
