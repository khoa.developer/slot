<html>
  <head>
    <link rel="stylesheet" type="text/css" href="add.css" />
    <script
  src="http://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="main-box">
      <form action="add_session.php" method="post">
        <div class="left-box">
          <div class="form-group">
            <label>Code chương trình:</label>
            <input type="text" name="code" />
          </div>
          <div class="form-group">
            <label>Tên chương trình:</label>
            <input type="text" name="name" />
          </div>
          <div class="form-group">
            <label>Ngày:</label>
            <input type="date" name="date" />
          </div>
          <div class="form-group">
            <label>Mô tả:</label><br />
            <textarea name="desc"></textarea>
          </div>
          <div class="form-group">
            <label>Danh sách tham gia:</label><br />
            <textarea id="join-list" name="joinlist"></textarea>
          </div>
        </div>
        <div class="right-box">
          <div class="right-top">
            <div class="reward-container">
              <div class="left-part">
                <button type="button" class="btn-delete-reward">X</button>
              </div>
              <div class="right-part">
                <div class="form-group">
                  <label>Giải thưởng:</label>
                  <input type="text" name="reward[]"/>
                </div>
                <div class="form-group">
                  <label>Số lượng</label>
                  <input type="number" name="quan[]" />
                </div>
              </div>
            </div>
            <div class="reward-container">
              <div class="left-part">
                <button type="button" class="btn-delete-reward">X</button>
              </div>
              <div class="right-part">
                <div class="form-group">
                  <label>Giải thưởng:</label>
                  <input type="text" name="reward[]"/>
                </div>
                <div class="form-group">
                  <label>Số lượng</label>
                  <input type="number" name="quan[]" />
                </div>
              </div>
            </div>
          </div>
          <div class="right-bottom">
            <input type="text" value="add" name="action" style="display: none;"/> 
            <button id="btn-add-reward" type="button">Thêm giải</button>
            <button id="btn-add-session">Thêm chương trình</button>
          </div>
        </div>
      </form>
    </div>

    <script>
      $('#btn-add-reward').on('click', function() {
        $('.right-top').append('<div class="reward-container">\
          <div class="left-part">\
            <button type="button" class="btn-delete-reward">X</button>\
          </div>\
          <div class="right-part">\
            <div class="form-group">\
              <label>Giải thưởng:</label>\
              <input type="text" name="reward[]"/>\
            </div>\
            <div class="form-group">\
              <label>Số lượng</label>\
              <input type="number" name="quan[]" />\
            </div>\
          </div>\
        </div>')
      });

      $(document).on('click', '.btn-delete-reward',  function() {
        $(this).closest('.reward-container').remove();
      });
    </script>
  </body>
</html>
