<?php
  include 'globals.php';
  include 'read_files.php';
 ?>
<html>
  <head>
    <title>Manage</title>
    <link rel="stylesheet" type="text/css" href="manage.css" />
    <script
  src="http://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="navigation">
      <form action="add.php">
        <button>Thêm mới</button>
      </form>
    </div>
    <div class="main-box">
      <?php
      $test = read_data($data_path);
        foreach ($test as $value) {
          echo "<div class='slot-session'>
            <div class='session-meta'>
              <span class='session-name'>".$value["name"]."</span> <span class='session-date'>".date('d/m/Y', $value["date"])."</span>
            </div>
            <div class='session-desc'>
              ".$value["desc"]."
            </div>
            <div class='session-actions'>
              <a href='home.php?code=".$value["code"]."'>Bốc thăm</a>&nbsp;
              <a href='edit_session.php?code=".$value["code"]."'>Sửa</a>&nbsp;
              <a href='delete_session.php?code=".$value["code"]."' class='btn-delete-session' id='".$value["code"]."'>Xóa</a>&nbsp;
            </div>
          </div>";
        }
       ?>
    </div>

    <script>
      $('.btn-delete-session').on('click', function(event) {
        var ok = confirm("Xác nhận xóa chương trình khuyến mãi?");
        if (!ok) {
          event.preventDefault();
        }
      });
    </script>
  </body>
</html>
