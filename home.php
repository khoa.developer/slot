<?php
  include 'globals.php';
  include 'read_files.php';

  $session_data = read_session($data_path, $_GET['code']);
  $win = $session_data["joinlist"][array_rand($session_data["joinlist"])];
  $chars = str_split($win);
  $word_index = array(array(-1));
  switch ($chars[0]) {
    case 'A':
      $word_index[0][0] = 0;
      break;
    case 'B':
      $word_index[0][0] = 1;
      break;
    case 'C':
      $word_index[0][0] = 2;
      break;
    case 'D':
      $word_index[0][0] = 3;
      break;
  }

  $table_num = array(array(-1, -1, -1));


  for($i = 1; $i <= 3; $i++) {
    $table_num[0][$i - 1] = (int)$chars[$i];
  }

 ?>
<html>
    <head>
        <title>Bốc thăm trúng thưởng</title>
        <meta charset="utf-8"/>

        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.1/css/bootstrap.min.css" rel="stylesheet"/>
        <link rel="stylesheet" type="text/css" href="home.css"/>
        <style>
          ul {
            padding: 0;
            margin: 0;
            list-style: none;
          }

          body {
            overflow: hidden;
          }

          .jSlots-wrapper {
            overflow: hidden;
            height: 20px;
            display: inline-block; /* to size correctly, can use float too, or width*/
            border: 1px solid #999;
          }

          .slot {
            float: left;
          }

          /* ---------------------------------------------------------------------
           FANCY EXAMPLE
          --------------------------------------------------------------------- */
           .jSlots-wrapper {
            overflow: hidden;
            height: 100px;
            display: inline-block; /* to size correctly, can use float too, or width*/
            border: 1px solid #999;
          }

          .slot li {
            width: 100px;
            line-height: 100px;
            text-align: center;
            font-size: 70px;
            font-weight: bold;
            color: #fff;
            text-shadow: 1px 1px 0 rgba(0, 0, 0, 0.8);
            font-family: 'Gravitas One', serif;
            border-left: 1px solid #999;
          }

          .slot:first-child li {
            border-left: none;
          }

          .slot li:nth-child(7) {
            background-color: #FFCE29;
          }
          .slot li:nth-child(6) {
            background-color: #FFA22B;
          }
          .slot li:nth-child(5) {
            background-color: #FF8645;
          }
          .slot li:nth-child(4) {
            background-color: #FF6D3F;
          }
          .slot li:nth-child(3) {
            background-color: #FF494C;
          }
          .slot li:nth-child(2) {
            background-color: #FF3333;
          }
          .slot li:nth-child(1),
          .slot li:nth-child(8) {
            background-color: #FF0000;
          }

          .slot li span {
            display: block;
          }

          .slot-machine {
            margin-top: 200px;
          }
        </style>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-sidebar/3.3.2/jquery.sidebar.min.js"></script>
        <script src="jquery.jSlots.js"></script>
        <script src="jquery.easing.1.3.js"></script>
    </head>
    <body>
        <div class="header">
            <div class="icon">
                <img src="dice.png"/>
            </div>
            <div class="header-info">
                <h1>Chương trình bốc thăm</h1>
                <p><?php echo $session_data['name']; ?></p>
            </div>
        </div>
        <div class="main">
          <div class="slot-zone" <?php if ($_GET["reward"] == null) { echo 'style="visibility: hidden;"';}  ?>>
            <div class="slot-machine machine-2">
              <ul class="slot slot-a">
                <li>A</li>
                <li>B</li>
                <li>C</li>
                <li>D</li>
              </ul>
              <ul class="slot slot-b">
                <li>0</li>
                <li>1</li>
                <li>2</li>
                <li>3</li>
                <li>4</li>
                <li>5</li>
                <li>6</li>
                <li>7</li>
                <li>8</li>
                <li>9</li>
              </ul>
            </div>
            <div class="slot-play">
              <input type="button" value="Quay" id="playBtn" />
            </div>
            <div class="slot-result-navigate">
              <button id="btn-save">Lưu</button>
              <button id="btn-replay">Bốc lại</button>
            </div>
          </div>
          <div class="rewards-zone">
            <?php
              $reward_index = 0;
              foreach ($session_data['rewards'] as $reward) {
                $winner_string = "";
                if (array_key_exists('winners', $reward)) {
                  foreach ($reward['winners'] as $winner) {
                    $winner_string .= "<b>" . $winner . "</b>" . '<br/>';
                  }
                } else {
                  $winner_string = "<b>Chưa bốc thăm</b>";
                }

                $slot_reward_button = "";
                if (!array_key_exists('winners', $reward) || (count($reward['winners']) != $reward['quan'])) {
                  $slot_reward_button = "<a href='home.php?code=".$session_data['code']."&reward=".$reward_index."'>Bốc thăm giải</a>";
                }

                echo '<div class="reward-container">
                  <p>
                    Giải thưởng: <b>'.$reward['reward'].'</b><br />
                    Số lượng: '.$reward['quan'].' giải<br />
                    Kết quả:<br />
                    '.$winner_string.' <br />
                    '.$slot_reward_button.'
                  </p>
                </div>';

                $reward_index++;
              }
             ?>
          </div>
        </div>
        <script>
            $(".sidebar.bottom").sidebar({side: "bottom"});
            $(".btn[data-action]").on("click", function () {
                var $this = $(this);
                var action = $this.attr("data-action");
                var side = $this.attr("data-side");
                $(".sidebar." + side).trigger("sidebar:" + action);
                console.log('asdfasdf');
                return false;
            });
            $('.slot-a').jSlots({
              number : 1,
              spinner : '#playBtn',
              easing : 'easeOutSine',
              time : 7000,
              loops : 6,
              endNumbers: <?php echo json_encode($word_index) ?>
            });
            $('.slot-b').jSlots({
              number : 3,
              winnerNumber : 1,
              spinner : '#playBtn',
              easing : 'easeOutSine',
              time : 7000,
              loops : 6,
              endNumbers: <?php echo json_encode($table_num) ?>,
              onStart : function() {
                  $('.slot').removeClass('winner');
              },
              onWin : function(winCount, winners) {
                  // only fires if you win

                  $.each(winners, function() {
                      this.addClass('winner');
                  });

                  // react to the # of winning slots
                  if ( winCount === 1 ) {
                      //alert('You got ' + winCount + ' 7!!!');
                  } else if ( winCount > 1 ) {
                      //alert('You got ' + winCount + ' 7’s!!!');
                  }

              },
              onEnd: function(finalNumbers) {
                $('.slot-result-navigate').show();
                $('.slot-play').hide();
                $('#btn-save').on('click', function() {
                  var ok = confirm("Xác nhận lưu kết quả?");
                  if (ok) {
                    old_wins = JSON.parse('<?php echo json_encode($session_data['rewards']); ?>');
                    if (old_wins[<?php echo $_GET['reward'] ?>].winners === undefined) {
                      old_wins[<?php echo $_GET['reward'] ?>].winners = [];
                    }
                    old_wins[<?php echo $_GET['reward'] ?>].winners.push("<?php echo($win); ?>");
                    $.ajax({
                        type: "POST",
                        url: 'add_session.php',
                        data: {
                          code: "<?php echo $session_data['code'] ?>",
                          rewards: old_wins,
                          action: "save",
                          win: "<?php echo($win); ?>"
                        },
                        success: function(data) {
                          //document.write(data);
                          location.reload();
                        }
                    });
                  }
                });
              }
          });

          $("#btn-replay").click(function(){
            location.reload();
          });
        </script>
    </body>
</html>
