<?php
  error_reporting(0);

  $data_path = "./data/";

  function removeDirectory($path) {
    $files = glob($path . '/*');
    foreach ($files as $file) {
        is_dir($file) ? removeDirectory($file) : unlink($file);
    }
    rmdir($path);
    return;
  }
 ?>
